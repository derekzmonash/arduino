#include <Arduino_LSM9DS1.h>
#include <Arduino_HTS221.h>


unsigned long currentMillis = 0;
unsigned long previousMillis = 0;

float deltaT = 0;
int count = 0;

float x, y, z;
float xa, ya, za;
int r,g,b, a;
int sensorval,sensorval2;
int state = 1;
float temperature;

char startMarker = '<';
char endMarker = '>';
char initializeMarker = '^';


void setup() {
  pinMode(LEDR, OUTPUT);
  pinMode(LEDG, OUTPUT);
  pinMode(LEDB, OUTPUT);
  IMU.begin();
  HTS.begin();
  Serial.begin(115200);
  while (!Serial);
  Serial.print(initializeMarker);   //inializes the entire transmition
  previousMillis = millis();
}

void loop() {

  temperature = HTS.readTemperature();
  sensorval = analogRead(A0) ;
  sensorval2 = analogRead(A1) ;

  if(state==1){
    digitalWrite(LEDR, HIGH);
    digitalWrite(LEDG, LOW);
    digitalWrite(LEDB, LOW);
    state=2;
  }

  else if(state==2){
    digitalWrite(LEDR, LOW);
    digitalWrite(LEDG, HIGH);
    digitalWrite(LEDB, LOW);
    state=3; 
  }
  else if(state==3){
    digitalWrite(LEDR, LOW);
    digitalWrite(LEDG, LOW);
    digitalWrite(LEDB, HIGH);
    state=1;
  }
  
  if (IMU.gyroscopeAvailable()) {
    IMU.readGyroscope(x, y, z);  
  }

  if (IMU.accelerationAvailable()) {
    IMU.readAcceleration(xa, ya, za);   
  }
  
  serialOutput();
  currentMillis = millis();
  deltaT = (currentMillis - previousMillis) * .001;
  previousMillis = millis();
  delay(100); //keep from overloading Serial port
  if(count==100)
  {
    Serial.end();
  }
  count = count+1;
}

void serialOutput() {
  Serial.print(startMarker);
  Serial.print(temperature);
  Serial.print(",");
  Serial.print(sensorval);
  Serial.print(",");
  Serial.print(sensorval2);
  Serial.print(",");
  Serial.print(x);
  Serial.print(",");
  Serial.print(y);
  Serial.print(",");
  Serial.print(z);
  Serial.print(",");
  Serial.print(za);
  Serial.print(",");
  Serial.print(ya);
  Serial.print(",");
  Serial.print(za);
  Serial.print(",");
  Serial.print(deltaT, 3);          //sends the time it takes the loop to run in seconds down to 3 decimal places
  Serial.print(endMarker);
}
