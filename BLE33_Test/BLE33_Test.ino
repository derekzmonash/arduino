#include <Arduino_LSM9DS1.h>
#include <Battery.h>


Battery batt = Battery(4500, 6000, A0);
void setup() {
  // put your setup code here, to run once:

  Serial.begin(9600);
  pinMode(LEDR, OUTPUT);
  pinMode(LEDG, OUTPUT);
  pinMode(10, OUTPUT);
  IMU.begin();

  if (!IMU.begin()) {
    Serial.println("Failed to initialize IMU!");
    while (1);
  }
  Serial.print("Gyroscope sample rate = ");
  Serial.print(IMU.gyroscopeSampleRate());
  Serial.println(" Hz");
  Serial.println();

  batt.begin(3300, 2, &sigmoidal);
 
}

void loop() {
  // put your main code here, to run repeatedly
  
  
  int sensorval = analogRead(A1) ;
  /*
  Serial.print(" Strain1 = ");
  Serial.print(sensorval);

  int sensorval1 = analogRead(A5) ;
  Serial.print(" Strain2 = ");
  Serial.print(sensorval1);

  int sensorval2 = analogRead(A6) ;
  Serial.print(" Strain3 = ");
  Serial.print(sensorval2);

  */
  Serial.print("Battery voltage is ");
  Serial.print(batt.voltage());
  Serial.print(" (");
  Serial.print(batt.level());
  Serial.println("%)");
  if(sensorval <= 800){
    digitalWrite(10,HIGH);
  }
  else
  {
    digitalWrite(10,LOW);
  }
  
  /*float accx,accy,accz;
        //Accelerometer check
  if (IMU.accelerationAvailable()) {
        IMU.readAcceleration(accx, accy, accz);
        Serial.print(" AccX: "); // print it
        Serial.print(accx);
        Serial.print(" AccY: "); // print it
        Serial.print(accy);
        Serial.print(" AccZ: "); // print it
        Serial.print(accz);
        
  }


  float gyrox,gyroy,gyroz;
  if (IMU.gyroscopeAvailable()) {
        IMU.readGyroscope(gyrox, gyroy, gyroz);
        Serial.print(" GyroX: "); // print it
        Serial.print(gyrox);
        Serial.print(" GyroY: "); // print it
        Serial.print(gyroy);
        Serial.print(" GyroZ: "); // print it
        Serial.println(gyroz);
        
        
  }*/
  delay(100);
  
  //digitalWrite(10,LOW);
  
  //delay(1000);

 }
