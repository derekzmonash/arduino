#include <ArduinoBLE.h>
#include <Arduino_LSM9DS1.h>



 // BLE Battery Service
BLEService battService("000018A1-1100-1000-8000-00805F9B34FB");
BLEService eskinService("000018A2-1100-1000-8000-00805F9B34FB");

BLEService accService("000018A4-1100-1000-8000-00805F9B34FB");
BLEService gyroService("000018A7-1100-1000-8000-00805F9B34FB");

BLEService postureService("000018A8-1100-1000-8000-00805F9B34FB");
BLEService timeService("000018A9-1100-1000-8000-00805F9B34FB");

// BLE Battery Level Characteristic
BLEIntCharacteristic battChar("00008AB1-1100-1000-8000-00805F9B34FB",  // standard 16-bit characteristic UUID
    BLERead | BLENotify); // remote clients will be able to get notifications if this characteristic changes
BLELongCharacteristic eskinChar("00008AB3-1100-1000-8000-00805F9B34FB",  // standard 16-bit characteristic UUID
    BLERead | BLENotify); // remote clients will be able to get notifications if this characteristic changes

    
BLELongCharacteristic accChar("00008AB4-1100-1000-8000-00805F9B34FB",  // standard 16-bit characteristic UUID
    BLERead | BLENotify); // remote clients will be able to get notifications if this characteristic changes


BLEIntCharacteristic gyroXChar("00008AB7-1100-1000-8000-00805F9B34FB",  // standard 16-bit characteristic UUID
    BLERead | BLENotify); // remote clients will be able to get notifications if this characteristic changes
BLEIntCharacteristic gyroYChar("00008AB8-1100-1000-8000-00805F9B34FB",  // standard 16-bit characteristic UUID
    BLERead | BLENotify); // remote clients will be able to get notifications if this characteristic changes
BLEIntCharacteristic gyroZChar("00008AB9-1100-1000-8000-00805F9B34FB",  // standard 16-bit characteristic UUID
    BLERead | BLENotify); // remote clients will be able to get notifications if this characteristic changes
          
BLEIntCharacteristic postChar("00008AC0-1100-1000-8000-00805F9B34FB",  // standard 16-bit characteristic UUID
    BLERead | BLENotify); // remote clients will be able to get notifications if this characteristic changes 
BLELongCharacteristic timeChar("00008AC1-1100-1000-8000-00805F9B34FB",  // standard 16-bit characteristic UUID
    BLERead | BLENotify); // remote clients will be able to get notifications if this characteristic changes 

int battupdate =0 ; 
long previousMillis = 0;
int movementmonitor;
int movementpast[]={0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
int moveid=0;
int posturebypass,posturebypassacc;
int posturestatus;
int posturepast []={1,1,1,1,1};
int posturestatusrecord;
int postureavg,postureskipflag;
int postureid=0;

long accbt;

float alpha = 0.828;
float batteryvoltbt;
float accx,accy,accz;
int eskinLfilt,eskinRfilt;
float accxfil,accyfil,acczfil;
float gyrox,gyroy,gyroz;
float gyroxfil,gyroyfil,gyrozfil;


void setup() {
  Serial.begin(9600);    // initialize serial communication
  
  pinMode(LED_BUILTIN, OUTPUT); // initialize the built-in LED pin to indicate when a central is connected
  IMU.begin();
  // begin initialization
  if (!BLE.begin()) {
    Serial.println("starting BLE failed!");

    while (1);
  
  eskinLfilt = analogRead(A4) ;
  eskinRfilt = analogRead(A1) ;
  

  
  if (IMU.accelerationAvailable()) {
        IMU.readAcceleration(accxfil, accyfil, acczfil);
        
  }

  if (IMU.gyroscopeAvailable()) {
        IMU.readGyroscope(gyroxfil, gyroyfil, gyrozfil);
                
  }
  
  }

  /* Set a local name for the BLE device
     This name will appear in advertising packets
     and can be used by remote devices to identify this BLE device
     The name can be changed but maybe be truncated based on space left in advertisement packet
  */
  BLE.setDeviceName( "Arduino Nano 33 BLE" );
  BLE.setLocalName("PosturePrototype");
  BLE.setAdvertisedService(battService); // add the service UUID
  BLE.setAdvertisedService(eskinService); // add the service UUID
  BLE.setAdvertisedService(accService); // add the service UUID
  BLE.setAdvertisedService(gyroService); // add the service UUID
  BLE.setAdvertisedService(postureService); // add the service UUID
  
  battService.addCharacteristic(battChar); // add the battery level characteristic
  eskinService.addCharacteristic(eskinChar); 
  accService.addCharacteristic(accChar); // add the accelerometer characteristic
  gyroService.addCharacteristic(gyroXChar); // add the gyro characteristic
  gyroService.addCharacteristic(gyroYChar);
  gyroService.addCharacteristic(gyroZChar); 
  postureService.addCharacteristic(postChar);
  timeService.addCharacteristic(timeChar);
  
  BLE.addService( battService );
  BLE.addService( eskinService );
  BLE.addService( accService );
  BLE.addService( gyroService );
  BLE.addService(postureService);
  BLE.addService(timeService);

  battChar.writeValue(0); // set initial value for this characteristic
  eskinChar.writeValue(0);
  gyroXChar.writeValue(5);
  gyroYChar.writeValue(5);
  gyroZChar.writeValue(5);
  accChar.writeValue(0);
  postChar.writeValue(0);
  timeChar.writeValue(0);

  float batteryvolt = analogRead(A5);
  float batteryvoltbt = (batteryvolt/1024)*3.3*2*100;
  battChar.writeValue(int(batteryvoltbt));  // and update the battery level characteristic


  
  /* Start advertising BLE.  It will start continuously transmitting BLE
     advertising packets and will be visible to remote BLE central devices
     until it receives a new connection */

  // start advertising
  BLE.advertise();

  Serial.println("Bluetooth device active, waiting for connections...");
}

void loop() {
  // wait for a BLE central
  BLEDevice central = BLE.central();
  Serial.println("Wait");
  long currentMillis = millis();
  
  if (central){   
    Serial.print("Connected to central: ");
    // print the central's BT address:
    Serial.println(central.address());
    // turn on the LED to indicate the connection:
    digitalWrite(LED_BUILTIN, HIGH);
    
    // check the battery level every 200ms
    // while the central is connected:
    while (central.connected()) {
      long currentMillis = millis();
      
      // if 200ms have passed, check all variables:
      if (currentMillis - previousMillis >= 200) {
        previousMillis = currentMillis;
        digitalWrite(LEDG,HIGH);

        posturemain(1);    
        
        digitalWrite(LEDG,LOW);
      }
        
    }
    // when the central disconnects, turn off the LED:
    digitalWrite(LED_BUILTIN, LOW);
    Serial.print("Disconnected from central: ");
    Serial.println(central.address());
  }
  else{
    if (currentMillis - previousMillis >= 200) {
        previousMillis = currentMillis;
        digitalWrite(LEDG,HIGH);

        posturemain(0);    
        
        digitalWrite(LEDG,LOW);
      }
  }

}

//Classification Function - Based on Boolean Naive Bayes Classifier
int postureclassifier(int eskinl, int eskinr, float accx){
  
  float eskinlgood, eskinlbad, eskinrgood, eskinrbad, accxgood, accxbad; 
  float goodprob, badprob;
  int posturestatus;

  //Determining if features exceed threshold or not, and assign predetermined probabilitites
  if(eskinl < 830){
    eskinlgood = 0.1;
    eskinlbad = 0.842;
    
  }
  else{
    eskinlgood = 0.9;
    eskinlbad = 0.158; 
  }

  if(eskinr < 830){
    eskinrgood = 0.011;
    eskinrbad = 0.846;
    
  }
  else{
    eskinrgood = 0.989;
    eskinrbad = 0.154; 
  }

  if(accx<0.7){
    accxgood = 0.032;
    accxbad = 0.995;
  }
  else{
    accxgood = 0.997;
    accxbad = 0.005;
  }

  //Calculate total good and bad probabilities
  goodprob = eskinlgood*eskinrgood*accxgood;
  badprob = eskinlbad*eskinrbad*accxbad;

  //Check which is larger to assign probability. 1=good, 2=bad
  if(goodprob<badprob){
    posturestatus = 2;
  }
  else{
    posturestatus = 1;
  }

  return posturestatus;
}

void posturemain(int ble){
  
  //Serial.println(previousMillis);
        
  //Updates the voltage check every 5 cycles
  if(battupdate == 5){
    float batteryvolt = analogRead(A5);
    float batteryvoltbt = (batteryvolt/1024)*3.3*2*100;
    
    battupdate=0;
  }
  else{
    battupdate++;
  }      

  //Eskin Check
  int eskinL = analogRead(A4);
  eskinLfilt = alpha * (eskinL) + (1- alpha)*eskinLfilt;
  
  int eskinR = analogRead(A1);
  eskinRfilt = alpha * (eskinR) + (1- alpha)*eskinRfilt;
  long eskinbt = eskinLfilt*10000+eskinRfilt;
  

  //Gyroscope check
  if (IMU.gyroscopeAvailable()) {
  IMU.readGyroscope(gyrox, gyroy, gyroz);
  gyroxfil = alpha * gyrox + (1- alpha)*gyroxfil;
  gyroyfil = alpha * gyroy + (1- alpha)*gyroyfil;
  gyrozfil = alpha * gyroz + (1- alpha)*gyrozfil;
  gyrox = gyroxfil *100;
  gyroy = gyroyfil *100;
  gyroz = gyrozfil *100;
  
  //Check if gyroscope values are significant, and add value to history of values
  if(abs(gyroxfil)>10|| abs(gyroxfil)>10 || abs(gyrozfil)>10){
    movementmonitor =1;
  }
  else{
    movementmonitor =0;
  }
  movementpast[moveid] = movementmonitor;
  
  //Sum up past 15 samples and if it is high, set classifier bypass flag high
  float movementavg=0;
  for(int i=0; i<=14;i++){
    movementavg=movementavg+movementpast[i];
  }
  if(movementavg>10){
    posturebypass = 1;
  }
  else{
    posturebypass =0;
  }
  //Serial.println(posturebypass);
  
  //Tracks and resets position of movement history array to be written
  if(moveid<14){
    moveid++;
  }
  else{
    moveid=0;
  }       
  
  }
  
  //Accelerometer check
  if (IMU.accelerationAvailable()) {
    IMU.readAcceleration(accx, accy, accz);
  
    accxfil = alpha * accx + (1- alpha)*accxfil;
    accyfil = alpha * accy + (1- alpha)*accyfil;
    acczfil = alpha * accz + (1- alpha)*acczfil;
    
    int accx = accxfil *100;
    int accy = accyfil*100;
    int accz = acczfil*100;
    
    //Combine values into one bluetooth value
    if(accx <0){
      accx = 1000+accx;
    }
    if(accy <0){
      accy = 1000+accy;
    }
    if(accz <0){
      accz = 1000+accz;
    }
  
    accbt = accx*1000000+accy*1000+accz;
    
    
  
    //Check if accx value is low enough, which means subject is bending over or lying down. If so, set bypass flag to high
    if(accxfil <0.2){
      posturebypassacc = 1;
      
    }
    else{
      posturebypassacc=0;
    }
    Serial.println(posturebypassacc);
  }

  //Classifier section
  //If bypass flag is raised, skip classification and set posture value to ???/invalid
  if(posturebypass ==1 || posturebypassacc ==1){
    posturestatus = 0;
    postureskipflag=1;
    posturestatusrecord = 0;
  }
  else{
    postureskipflag=0;
    posturestatusrecord= postureclassifier(eskinLfilt, eskinRfilt,accxfil);
    
  }

  if(posturestatusrecord == 0 || posturestatusrecord ==1){
    posturepast[postureid] = 1;
  }
  else{
    posturepast[postureid] = 0;
  }
  
  Serial.print("Posture hist:");
  Serial.print(posturepast[0]);
  Serial.print(posturepast[1]);
  Serial.print(posturepast[2]);
  Serial.print(posturepast[3]);
  Serial.println(posturepast[4]);
  
  //Sum up past 5 samples and if result is above 2, therefore overall posture is good, else bad
  float postureavg=0;
  for(int i=0; i<=4;i++){
    postureavg=postureavg+posturepast[i];
  }
  Serial.println(posturepast[4]);
  if(postureskipflag==1){
    posturestatus = 0;
  }
  else{
    if(postureavg > 2){
      posturestatus = 1;
    }
    else{
      posturestatus=2;
    }
  }
  
  if(postureid<4){
    postureid++;
  }
  else{
    postureid=0;
  }   
  Serial.print("Posture :");
  Serial.println(posturestatus);
  
  


  if(ble){
    timeChar.writeValue(previousMillis);
    //battChar.writeValue(int(batteryvoltbt));  // and update the battery level characteristic
    eskinChar.writeValue(eskinbt);
    //gyroXChar.writeValue(int(gyrox));
    //gyroYChar.writeValue(int(gyroy));
    //gyroZChar.writeValue(int(gyroz));
    accChar.writeValue(accbt);
    postChar.writeValue(posturestatus);
  }
  else{
    //if posture bad, trigger vibration motor
    if(posturestatus ==2){
      digitalWrite(10,HIGH);
    }
    else{
      digitalWrite(10,LOW);
    }  
  }
  
}
